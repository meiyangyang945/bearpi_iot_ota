---
typora-copy-images-to: assets
---

# BearPi_IOT_OTA

#### 介绍

![BearPi](assets/BearPi.jpg)

![ThingsBoard](assets/ThingsBoard.png)

![ui](assets/ui.jpg)

![ui](assets/ota.png)

使用小熊派STM32开发板实现的物联网项目，同时具备OTA远程升级功能，包含丰富的自定义项目：BIN文件管理，HTTPServer、MySQL数据管理、ThingsBoard等。

#### 准备材料

- 关于OTA课程的材料准备

- 硬件：小熊派开发板+E53IA1+Cat.1模组+4G物联网卡 

- 软件工具： 

- 串口工具--我提供给你们我已做好必要指令集 

- Postman--测试http请求 

- Stm32CubeProgrammer--可向STM32指定地址烧录文件，很重要！测试bootloader 

- MQTT.fx--测试MQTT通讯 

- 服务器类（以下非必须，可以用我的，不过不方便）： 

- linux云服务器 --建议最低2核1G 

- Nodered--可通过Docker一键安装（如果出现安装好以后，无法在线下载插件和连接MQTT，删除，采用非Docker方式安装） 

- ThingsBoard--可通过Docker一键安装 （软件比较大，不要装专业版） 

- mysql--用于用户管理与固件版本管理（和nodered配套使用




### 备注
> 目前代码处于开发阶段，应该每天都会更新，课程开始时间在群内公布：476840321
