#include "main_ui.h"

#include "lcd_spi2_drv.h"


/*显示基准1隐藏0*/
//void display_base(uint8_t enable)
//{
//    if(enable == 1)
//    {
//        Lcd_show_bmp(DETECT_LOGO_X, DETECT_LOGO_Y, B2_LOGO);
//        LCD_ShowChinese(94, 130, (uint8_t *)"基准", WHITE, BLACK, 24, 1);
//    }
//    else if(enable == 0)
//    {
//        LCD_Fill(DETECT_LOGO_X, DETECT_LOGO_Y, DETECT_LOGO_X + 84, DETECT_LOGO_Y + 84, BLACK);
//        LCD_Fill(94, 130, 94 + 48, 130 + 24, BLACK);
//    }
//}

/*显示检测1隐藏0*/
//void display_detect(uint8_t enable)
//{
//    if(enable == 1)
//    {
//        Lcd_show_bmp(DETECT_LOGO_X, DETECT_LOGO_Y, D2_LOGO);
//        LCD_ShowChinese(94, 130, (uint8_t *)"检测", WHITE, BLACK, 24, 1);
//    }
//    else if(enable == 0)
//    {
//        LCD_Fill(DETECT_LOGO_X, DETECT_LOGO_Y, DETECT_LOGO_X + 84, DETECT_LOGO_Y + 84, BLACK);
//        LCD_Fill(94, 130, 94 + 48, 130 + 24, BLACK);
//    }
//}

/*显示安全1隐藏0*/
//void display_safety(uint8_t enable)
//{
//    if(enable == 1)
//    {
//        Lcd_show_bmp(DETECT_LOGO_X, DETECT_LOGO_Y, R0_LOGO);
//        LCD_ShowChinese(94, 130, (uint8_t *)"安全", GREEN, BLACK, 24, 1);
//    }
//    else if(enable == 0)
//    {
//        LCD_Fill(DETECT_LOGO_X, DETECT_LOGO_Y, DETECT_LOGO_X + 84, DETECT_LOGO_Y + 84, BLACK);
//        LCD_Fill(94, 130, 94 + 48, 130 + 24, BLACK);
//    }
//}

/*显示危险1隐藏0*/
//void display_danger(uint8_t enable)
//{
//    if(enable == 1)
//    {
//        Lcd_show_bmp(DETECT_LOGO_X, DETECT_LOGO_Y, R1_LOGO);
//        LCD_ShowChinese(94, 130, (uint8_t *)"危险", RED, BLACK, 24, 1);
//    }
//    else if(enable == 0)
//    {
//        LCD_Fill(DETECT_LOGO_X, DETECT_LOGO_Y, DETECT_LOGO_X + 84, DETECT_LOGO_Y + 84, BLACK);
//        LCD_Fill(94, 130, 94 + 48, 130 + 24, BLACK);
//    }
//}

/*显示进度条框*/
void Display_Process_Bar_Frame(int enable)
{
    if(enable == 1)
    {
        LCD_Draw_ColorRect(10, 180, 230, 190, BLUE);
        LCD_Fill(12, 182, 228, 188, BLACK);
    }
    else if(enable == 0)
    {
        LCD_Draw_ColorRect(10, 180, 230, 190, BLACK);
        LCD_Fill(12, 182, 228, 188, BLACK);
    }
}

/*显示1隐藏0进度条*/
void Display_Process_Bar(int Process, int enable)
{
    uint8_t buf[] = {20, 40, 80, 100, 120, 140, 160, 180, 200, 228};

    if(enable == 1)
    {
        LCD_Fill(12, 182, buf[Process / 10], 188, GREEN);
    }
    else if(enable == 0)
    {
        Display_Process_Bar_Frame(enable);
        LCD_Fill(12, 182, buf[9], 188, BLACK);
    }
}

/*显示烟感值*/
//void display_smoke_value(int smoke_value, uint16_t color, uint8_t enable)
//{
//    char display_buf[20] = {0};
//    memset(display_buf, 0, 20);
//    sprintf(display_buf, "%4dppm",smoke_value);
//    if(enable == 1)
//        LCD_ShowCharStr(SMOKE_X, SMOKE_Y, 100, display_buf, BLACK, color, 24);
//    else if(enable == 0)
//        LCD_ShowCharStr(SMOKE_X, SMOKE_Y, 100, display_buf, BLACK, BLACK, 24);
//}


/*测试页面初始化*/
//void test_page_init(void)
//{
//    Flow_Cursor.flow_cursor = TEST_PAGE ;
//    /*传感器检测流程初始化*/
//    Sensor_Detect_Init();
//    /*显示基准*/
//    display_base(1);
//    display_smoke_value(0, GREEN, 1);
//    LCD_ShowChinese(40, 208, (uint8_t *)"按右键退出", GREEN, BLACK, 32, 1);
//}



