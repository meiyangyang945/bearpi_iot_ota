/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usart.h"
#include "iwdg.h"
#include "EC600S.h"
#include "lcd_spi2_drv.h"
#include "main_ui.h"
#include "E53_IA1.h"
#include "Sensor_work.h"
#include "OTA_Task.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
osThreadId defaultTaskHandle;
osThreadId EC600S_TaskHandle;
osThreadId Sensor_TaskHandle;
osThreadId OTA_TaskHandle;
osThreadId UI_TaskHandle;
osMessageQId EC600S_Rev_QueueHandle;
osMessageQId Sensor_QueueHandle;
osMessageQId MQTT_REV_QueueHandle;
osSemaphoreId OTA_StartHandle;

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartDefaultTask(void const * argument);
void Start_EC600S_Task(void const * argument);
void Start_Sensor_Task(void const * argument);
void Start_OTA_Task(void const * argument);
void Start_UI_Task(void const * argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/* GetIdleTaskMemory prototype (linked to static allocation support) */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );

/* USER CODE BEGIN GET_IDLE_TASK_MEMORY */
static StaticTask_t xIdleTaskTCBBuffer;
static StackType_t xIdleStack[configMINIMAL_STACK_SIZE];

void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
    *ppxIdleTaskTCBBuffer = &xIdleTaskTCBBuffer;
    *ppxIdleTaskStackBuffer = &xIdleStack[0];
    *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
    /* place for user code */
}
/* USER CODE END GET_IDLE_TASK_MEMORY */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
    /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* definition and creation of OTA_Start */
  osSemaphoreDef(OTA_Start);
  OTA_StartHandle = osSemaphoreCreate(osSemaphore(OTA_Start), 1);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
    /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
    /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* definition and creation of EC600S_Rev_Queue */
  osMessageQDef(EC600S_Rev_Queue, 2, 1200);
  EC600S_Rev_QueueHandle = osMessageCreate(osMessageQ(EC600S_Rev_Queue), NULL);

  /* definition and creation of Sensor_Queue */
  osMessageQDef(Sensor_Queue, 5, 200);
  Sensor_QueueHandle = osMessageCreate(osMessageQ(Sensor_Queue), NULL);

  /* definition and creation of MQTT_REV_Queue */
  osMessageQDef(MQTT_REV_Queue, 3, 200);
  MQTT_REV_QueueHandle = osMessageCreate(osMessageQ(MQTT_REV_Queue), NULL);

  /* USER CODE BEGIN RTOS_QUEUES */
    EC600S_Rev_QueueHandle =  xQueueCreate(2,1200);     //创建EC600S指令接收队列
    MQTT_REV_QueueHandle = xQueueCreate(3,200);         //创建MQTT订阅消息队列
    Sensor_QueueHandle = xQueueCreate(5,200);           //创建传感器上报消息队列
    /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  /* definition and creation of EC600S_Task */
  osThreadDef(EC600S_Task, Start_EC600S_Task, osPriorityHigh, 0, 2048);
  EC600S_TaskHandle = osThreadCreate(osThread(EC600S_Task), NULL);

  /* definition and creation of Sensor_Task */
  osThreadDef(Sensor_Task, Start_Sensor_Task, osPriorityIdle, 0, 1024);
  Sensor_TaskHandle = osThreadCreate(osThread(Sensor_Task), NULL);

  /* definition and creation of OTA_Task */
  osThreadDef(OTA_Task, Start_OTA_Task, osPriorityNormal, 0, 2048);
  OTA_TaskHandle = osThreadCreate(osThread(OTA_Task), NULL);

  /* definition and creation of UI_Task */
  osThreadDef(UI_Task, Start_UI_Task, osPriorityIdle, 0, 128);
  UI_TaskHandle = osThreadCreate(osThread(UI_Task), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
    /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

}

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void const * argument)
{
  /* USER CODE BEGIN StartDefaultTask */

    /* Infinite loop */
    for(;;)
    {
        // printf("\r\n-------------------------EC600S_TaskHandle栈空间的高水线:%ld\r\n",uxTaskGetStackHighWaterMark(EC600S_TaskHandle));
        // printf("\r\n-------------------------OTA_TaskHandle栈空间的高水线:%ld\r\n",uxTaskGetStackHighWaterMark(OTA_TaskHandle));
      osDelay(LED_Toggle_time);
      HAL_GPIO_TogglePin(GPIOC,LED_Pin);
			HAL_IWDG_Refresh(&hiwdg);

    }
  /* USER CODE END StartDefaultTask */
}

/* USER CODE BEGIN Header_Start_EC600S_Task */
/**
* @brief Function implementing the EC600S_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_EC600S_Task */
void Start_EC600S_Task(void const * argument)
{
  /* USER CODE BEGIN Start_EC600S_Task */
    int try_time = 0;
    uint8_t ucExecRes = 0;
    uint8_t publish_message[200]; //和消息队列大小对应
    uint8_t Subscribe_message[200]; //和消息队列大小对应
    EC600S_type.CSQ=0;
    vTaskSuspend(OTA_TaskHandle);     //挂起OTA任务 如果不升级的话 是用不到的
EC600S_RST:
    EC600S_Wait_Wakeup();
    EC600S_ATE0_EN();
Wait_Net:
    ucExecRes = EC600S_WAIT_CREG();
    if(ucExecRes!=Normal)     //超时可计次然后判错复位
    {
        osDelay(1000);
        try_time++;
        if(try_time>60)
        {
            try_time=0;
            printf("驻网超时....\r\n");
             goto EC600S_RST;
        }
        else {
            goto Wait_Net;
        }
    }
    EC600S_GET_CSQ();
    EC600S_GET_IMEI();
   ucExecRes = EC600S_GET_Version((uint8_t *)"http://39.107.239.44:1880/STM32_Version?imei=868540050007893");
		if(ucExecRes == 66) 
    EC600S_Get_Bin((uint8_t *)"http://39.107.239.44:1880/STM32OTA?imei=868540050007893");
Connect_MQTT:
    EC600S_Connect_MQTT((uint8_t*)"39.107.239.44",1883,(uint8_t *)"bear_pi",(uint8_t*)"bear_pi",(uint8_t*)"bear_pi");
    EC600S_Subscribe_MQTT((uint8_t*)"v1/devices/me/rpc/request/+",1);
    /* Infinite loop */
    for(;;)
    {
        if( EC600S_Check_Net() == 0)
        {
            memset(publish_message,0x00,sizeof(publish_message));
            if(xQueueReceive(Sensor_QueueHandle,publish_message,1000/portTICK_RATE_MS) == pdPASS) //等待消息队列
            {
                ucExecRes =  EC600S_Publish_MQTT((uint8_t *)"v1/devices/me/telemetry",publish_message,0);
                if (ucExecRes == ExecFail)
                {
                    goto Connect_MQTT;
                }
            }
        }
        else {
            printf("自检异常....\r\n");
        }
        if(xQueueReceive(MQTT_REV_QueueHandle,Subscribe_message,0/portTICK_RATE_MS) == pdPASS) //等待平台下发消息队列 立即响应 不影响其他业务执行
        {
            printf("平台下发消息：%s\r\n",Subscribe_message);
            if(strstr((char *)Subscribe_message,"open")!=NULL)
            {
                HAL_GPIO_WritePin(E53_GPIO1_GPIO_Port, E53_GPIO1_Pin, GPIO_PIN_SET);
                HAL_GPIO_WritePin(GPIOB,E530_GPIO3_Pin, GPIO_PIN_SET);
            } else
            {
                HAL_GPIO_WritePin(E53_GPIO1_GPIO_Port, E53_GPIO1_Pin, GPIO_PIN_RESET);
                HAL_GPIO_WritePin(GPIOB,E530_GPIO3_Pin, GPIO_PIN_RESET);

            }

            memset(Subscribe_message,0x00,sizeof(Subscribe_message));
        }
         osDelay(1); //关闭延时 直接起飞  小心流量哦 ！！！
    }
  /* USER CODE END Start_EC600S_Task */
}

/* USER CODE BEGIN Header_Start_Sensor_Task */
/**
* @brief Function implementing the Sensor_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_Sensor_Task */
void Start_Sensor_Task(void const * argument)
{
  /* USER CODE BEGIN Start_Sensor_Task */
    SHT30_Reset();
    if(SHT30_Init() == HAL_OK)
        printf("sht30 init ok.\n");
    else
        printf("sht30 init fail.\n");
    /* Infinite loop */
    for(;;)
    {
        Sensor_Task();
        osDelay(1);
    }
  /* USER CODE END Start_Sensor_Task */
}

/* USER CODE BEGIN Header_Start_OTA_Task */
/**
* @brief Function implementing the OTA_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_OTA_Task */
void Start_OTA_Task(void const * argument)
{
  /* USER CODE BEGIN Start_OTA_Task */
    BaseType_t err = pdFALSE;
    LED_Toggle_time = 100;
    /* Infinite loop */
    for(;;)
    {
        printf("OTA Task is runing......\r\n");
        err = xSemaphoreTake(OTA_StartHandle,portMAX_DELAY); //一直等信号量
        if(err == pdTRUE)  //获取信号量成功
        {
            OTA_Task();
        }
        osDelay(1);
    }
  /* USER CODE END Start_OTA_Task */
}

/* USER CODE BEGIN Header_Start_UI_Task */
/**
* @brief Function implementing the UI_Task thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_Start_UI_Task */
void Start_UI_Task(void const * argument)
{
  /* USER CODE BEGIN Start_UI_Task */
    LCD_Init();
    LCD_DisplayOn();
    display_base(1);
    /* Infinite loop */
    for(;;)
    {
        update_ui();
        osDelay(1);
    }
  /* USER CODE END Start_UI_Task */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */




/* USER CODE END Application */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
